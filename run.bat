@echo off
if not exist .\bin\node.exe (
    echo No NodeJS binary found, start first time running process

    reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

    if %OS%==32BIT echo Windows 32 bits, start download NodeJS x86
    if %OS%==32BIT curl https://nodejs.org/dist/v18.17.0/node-v18.17.0-win-x86.zip > node.zip
    if %OS%==64BIT echo Windows 64 bits, start download NodeJS x64
    if %OS%==64BIT curl https://nodejs.org/dist/v18.17.0/node-v18.17.0-win-x64.zip > node.zip

    echo Unziping NodeJS binary file
    powershell Expand-Archive .\node.zip -DestinationPath .\

    echo Cleaning
    move node-* bin
    del node.zip
    echo Done installation!
)

:: echo Checking and download dependancy
:: .\bin\npm -i

echo Starting application 
.\bin\node.exe .\index.js