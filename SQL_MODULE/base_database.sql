CREATE DATABASE [TPRL];
USE [TPRL];
/*  ---> All request much listed here, [REQUEST_STATE] is much have
    --->    - CREATED: created but not requested, waiting for sender send request to SFTP server
            - REQUESTED: request has been sent to SFTP server, and waiting for respond
            - RECEIVED: received ACK/NAK file
            - DONE: received DONE file, start clean process
            - COMPLETED: Request flow completed
    ---> All request much listed here, [REQUEST_STATUS] is much have
    --->    - CREATED: created but not requested, waiting for sender send request to SFTP server
            - REJECTED: Received NAK
            - ACCEPTED: Received ACK
            - DONE: Received DONE
    ---> SQL_CMD will be executed at step 2 (after completed request)
            - All SQL command need to be a single command, do not use a muiltiple command (key word GO or ; is not excepted).
            - If multiple command is required, use FUNCTION or PROCEDURE instead!!!*/
CREATE TABLE [REQUEST] (
	[REQUEST_ID] VARCHAR(14) PRIMARY KEY,
	[REQUEST_TYPE] VARCHAR(200) NOT NULL,
    [REQUEST_FILE_NAME] VARCHAR(200) NOT NULL,
	[CONTENT] NVARCHAR(MAX) NOT NULL,
	[REQUEST_STATUS] VARCHAR(10) NOT NULL,
	[ACK_FILE_NAME] VARCHAR(200),
	[ACK_CONTENT] NVARCHAR(MAX),
	[ACK_STATUS] VARCHAR(10),
	[NAK_FILE_NAME] VARCHAR(200),
	[NAK_CONTENT] NVARCHAR(MAX),
	[NAK_STATUS] VARCHAR(10),
	[DONE_FILE_NAME] VARCHAR(200),
	[DONE_CONTENT] NVARCHAR(MAX),
	[DONE_STATUS] VARCHAR(10),
    [REQUEST_STATE] VARCHAR(10) NOT NULL, --CREATED: created but not requested, waiting for sender send request to SFTP server - REQUESTED: request has been sent to SFTP server, and waiting for respond - RECEIVED: received ACK/NAK file - DONE: received DONE file, start clean process - COMPLETED: Request flow completed
	[SQL_CMD] NVARCHAR(MAX), --SQL_CMD will be executed at step 2 (after completed request) all SQL command need to be a single command, do not use a muiltiple command (key word GO or ; is not excepted). If multiple command is required, use FUNCTION or PROCEDURE instead!!!
    [REQUEST_CREATED_AT] DATETIME NOT NULL,
	[UPDATED_AT] DATETIME NOT NULL
);
CREATE TABLE [LOG] (
	[LOG_ID] BIGINT IDENTITY(1,1) PRIMARY KEY,
	[INSERT_TIME] DATETIME NOT NULL,
	[LOG_TYPE] VARCHAR(10) NOT NULL,
	[CONTENT] NVARCHAR(MAX) NOT NULL,
);
--This table is a clone table of [ACCOUNT], data will be inserted where ever [ACCOUNT] has a change (being insert or modified)
CREATE TABLE [ACCOUNT_HISTORICAL] (
	[ACCOUNTID] VARCHAR(10),
    [ACCOUNTNAME] VARCHAR(50) NOT NULL,
    [IDTYPE] VARCHAR(10) NOT NULL,
    [NATIONALITYCODE] VARCHAR(4) NOT NULL,
    [IDNO] VARCHAR(20) NOT NULL UNIQUE,
    [ISSUEDATE] DATE NOT NULL,
    [IDISSUEPLACE]  NVARCHAR(MAX) NOT NULL,
    [EMAIL] VARCHAR(50),
    [PHONE] VARCHAR(12),
    [ADDRESS] NVARCHAR(MAX),
    [CITY] NVARCHAR(20),
    [SHAREHOLDERTYPE] NVARCHAR(MAX) NOT NULL,
    [FIELDOFACTIVITY] NVARCHAR(MAX),
    [BUSSINESSTYPE] NVARCHAR(MAX),
    [PROFESSIONALREGISTRATIONCRITERIA]  NVARCHAR(MAX),
    [PINV] NVARCHAR(MAX),
    [PROFESSIONALDATESTART] DATE,
    [PROFESSIONALDATEEND] DATE,
    [CHANGEDATETIME] DATETIME NOT NULL,
    [INSERTDATETIME] DATETIME NOT NULL,
    PRIMARY KEY ([ACCOUNTID], [CHANGEDATETIME])
);
CREATE TABLE [ACCOUNT] (
	[ACCOUNTID] NVARCHAR(10) PRIMARY KEY,
    [ACCOUNTNAME] VARCHAR(50) NOT NULL,
    [IDTYPE] VARCHAR(10) NOT NULL,
    [NATIONALITYCODE] VARCHAR(4) NOT NULL,
    [IDNO] VARCHAR(20) NOT NULL UNIQUE,
    [ISSUEDATE] DATE NOT NULL,
    [IDISSUEPLACE]  NVARCHAR(MAX) NOT NULL,
    [EMAIL] VARCHAR(50),
    [PHONE] VARCHAR(12),
    [ADDRESS] NVARCHAR(MAX),
    [CITY] NVARCHAR(20),
    [SHAREHOLDERTYPE] NVARCHAR(MAX) NOT NULL,
    [FIELDOFACTIVITY] NVARCHAR(MAX),
    [BUSSINESSTYPE] NVARCHAR(MAX),
    [PROFESSIONALREGISTRATIONCRITERIA]  NVARCHAR(MAX),
    [PINV] NVARCHAR(MAX),
    [PROFESSIONALDATESTART] DATE,
    [PROFESSIONALDATEEND] DATE,
    [CHANGEDATETIME] DATETIME NOT NULL,
    [INSERTDATETIME] DATETIME NOT NULL
);