/** Node configuration */
require('events').EventEmitter.defaultMaxListeners = 0;

/** Libs */
const SFTPClient = require('ssh2-sftp-client');
const XLSX = require('xlsx');
const dateFormat = require('dateformat');
const MSSQL = require('mssql');
const FSPromises = require('fs/promises');

/** Global constants */
const PATH = './';
const MODULES_PATH = 'FIN_MODULE/';
const CONFIGURATION_FILE = 'config.json';
const LOG_FILE = 'logs.txt';
const REQUEST_FILE_FOLDER = 'REQUESTS/';
const FIN_TEMPLATE_FOLDER = 'FIN_TEMPLATE/';
const REQUEST_EXCEL = 'REQUEST_EXCEL/';
const REMOTE_ROOT_PATH = '/';
const REMOTE_SEND_FOLDER = 'Send/';
const PULSE_DATE_TIME_FORMAT = `UTC:yyyy-mm-dd'T'HH:MM:ss'Z'`;
const REMOTE_RECEIVE_FOLDER = `ReceiveRec/`;
const TMP_RECEIVE_FORDER = 'TMP_RECEIVE/'

/** Global variables */
//General setting
/** fin_file_remote_mode - change remote server type - has 3 values: LOCAL, SMB, SFTP - For using SMB, SMB drive must mounted to system drive and using LOCAL type instead of SMB*/
let setting = {
    debug_log: false,
    fin_file_remote_mode: 'SFTP',
    fin_file_remote_root_path : "/"
}

//Repeadable job will report running time here
let pulse = {
    start: '',
    database_connected: '',
    sender: '',
    request_listener: '',
    receive_listener: '',
}
//App flag
let flags = {
    configuration_load: false,
    database_connected: false,
    sender_init: false,
    request_listener_init: false,
    receive_listener_init: false,
    exit: false
};
//MSSQL connection string
let db_conn = {
    user: 'sa',
    password: '123456789',
    database: 'TPRL',
    server: '172.0.0.1',
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    },
    options: {
      encrypt: true, // for azure
      trustServerCertificate: true // change to true for local dev / self-signed certs
    }
};
//SFTP connection string
let sftp_conn = {
    host: '172.0.0.1',
    port: '22',
    username: 'sftp',
    password: '123456789'
};
//Wait time for repeadable functions, using millisecond
let job_timer = {
    request_listener: 30000, //Default 30s after finished
    request_sender: 10000, //Default 1s after finished
    receiver_listener: 30000 //Default 30s after finished
};
let db = new MSSQL.ConnectionPool(db_conn);
let modules = [];

/** Type define here */
/**
 * 
 * @typedef {logContent}
 * @property {string} log_type
 * @property {string} content
 */
/**
 * 
 * @typedef {result}
 * @property {string} status
 * @property {string} content
 * @property {string} description
 */

/** Functions define here */
/**
 * 
 * @param {string} content 
 * @param {bool} decode_mode
 */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function telexDeEnCode(content,decode_mode) {
  let encode_decode_table = {
    "đ":"?dd?",
    "ă":"?aw?",
    "â":"?aa?",
    "ê":"?ee?",
    "ô":"?oo?",
    "ơ":"?ow?",
    "ư":"?uw?",
    "á":"?as?",
    "ắ":"?aws?",
    "ấ":"?aas?",
    "é":"?es?",
    "ế":"?ees?",
    "ố":"?oos?",
    "ớ":"?ows?",
    "ú":"?us?",
    "ứ":"?uws?",
    "í":"?is?",
    "à":"?af?",
    "ằ":"?awf?",
    "ầ":"?aaf?",
    "è":"?ef?",
    "ề":"?eef?",
    "ò":"?of?",
    "ồ":"?oof?",
    "ờ":"?owf?",
    "ù":"?uf?",
    "ừ":"?uwf?",
    "ì":"?if?",
    "ả":"?ar?",
    "ẳ":"?awr?",
    "ẩ":"?aar?",
    "ẻ":"?er?",
    "ể":"?eer?",
    "ỏ":"?or?",
    "ổ":"?oor?",
    "ở":"?owr?",
    "ủ":"?ur?",
    "ử":"?uwr?",
    "ỉ":"?ir?",
    "ã":"?ax?",
    "ẵ":"?awx?",
    "ẫ":"?aax?",
    "ẽ":"?ex?",
    "ễ":"?eex?",
    "õ":"?ox?",
    "ỗ":"?oox?",
    "ỡ":"?owx?",
    "ũ":"?ux?",
    "ữ":"?uwx?",
    "ĩ":"?ix?",
    "ạ":"?aj?",
    "ặ":"?awj?",
    "ậ":"?aaj?",
    "ẹ":"?ej?",
    "ệ":"?eej?",
    "ọ":"?oj?",
    "ộ":"?ooj?",
    "ợ":"?owj?",
    "ụ":"?uj?",
    "ự":"?uwj?",
    "ị":"?ij?",
    "@":"(at)",
    "Đ":"?DD?",
    "Ă":"?AW?",
    "Â":"?AA?",
    "Ê":"?EE?",
    "Ô":"?OO?",
    "Ơ":"?OW?",
    "Ư":"?UW?",
    "Á":"?AS?",
    "Ắ":"?AWS?",
    "Ấ":"?AAS?",
    "É":"?ES?",
    "Ế":"?EES?",
    "Ố":"?OOS?",
    "Ớ":"?OWS?",
    "Ú":"?US?",
    "Ứ":"?UWS?",
    "Í":"?IS?",
    "À":"?AF?",
    "Ằ":"?AWF?",
    "Ầ":"?AAF?",
    "È":"?EF?",
    "Ề":"?EEF?",
    "Ò":"?OF?",
    "Ồ":"?OOF?",
    "Ờ":"?OWF?",
    "Ù":"?UF?",
    "Ừ":"?UWF?",
    "Ì":"?IF?",
    "Ả":"?AR?",
    "Ẳ":"?AWR?",
    "Ẩ":"?AAR?",
    "Ẻ":"?ER?",
    "Ể":"?EER?",
    "Ỏ":"?OR?",
    "Ổ":"?OOR?",
    "Ở":"?OWR?",
    "Ủ":"?UR?",
    "Ử":"?UWR?",
    "Ỉ":"?IR?",
    "Ã":"?AX?",
    "Ẵ":"?AWX?",
    "Ẫ":"?AAX?",
    "Ẽ":"?EX?",
    "Ễ":"?EEX?",
    "Õ":"?OX?",
    "Ỗ":"?OOX?",
    "Ỡ":"?OWX?",
    "Ũ":"?UX?",
    "Ữ":"?UWX?",
    "Ĩ":"?IX?",
    "Ạ":"?AJ?",
    "Ặ":"?AWJ?",
    "Ậ":"?AAJ?",
    "Ẹ":"?EJ?",
    "Ệ":"?EEJ?",
    "Ọ":"?OJ?",
    "Ộ":"?OOJ?",
    "Ợ":"?OWJ?",
    "Ụ":"?UJ?",
    "Ự":"?UWJ?",
    "Ị":"?IJ?"        
}
  for(let i = 0; i < Object.keys(encode_decode_table).length; i++) {
      if (decode_mode == true) {
          if (content.includes(encode_decode_table[Object.keys(encode_decode_table)[i]]) == true) {
              content = content.replaceAll(encode_decode_table[Object.keys(encode_decode_table)[i]],Object.keys(encode_decode_table)[i]);
          }
      }
      else {
          if (content.includes(Object.keys(encode_decode_table)[i]) == true) {
              content = content.replaceAll(Object.keys(encode_decode_table)[i],encode_decode_table[Object.keys(encode_decode_table)[i]]);
          }
      }
  }
  return content;
}

/**
* 
* @param {string} content
* @returns {json} 
*/
async function fromFin(content) {
    let result = {};
    let lines = content.split('\n:');

    for (let i = 0; i < lines.length; i++) {
        //Skip first line, skip tag 16S and tag 16R
        if (i == 0 || lines[i].includes('16S') == true || lines[i].includes('16R') == true) {
            continue;
        }

        //Process last line
        if (i == (lines.length - 1)) {
            lines[i] = lines[i].split('\n');
            lines[i] = lines[i].slice(0,lines[i].length-1).join('\n');
        }

        //If not first line, then process
        let tmp = lines[i].split(':');
        if(tmp.length == 2) {
            if (tmp[1].split('/').length > 1){
                result[`:${tmp[0]}:`] = tmp[1].split('/');
            }
            else{
                result[`:${tmp[0]}:`] = tmp[1];
            }
        }
        else if(tmp.length == 3) {
            let tmp_2 = tmp[2].split('\n');
            if (result[`:${tmp[0]}:`] === undefined || result[`:${tmp[0]}:`] === null){
                result[`:${tmp[0]}:`] = {};
            }
            for (let j = 0; j < tmp_2.length; j++) {
                let tmp_3 = tmp_2[j].split('//');
                //No sub-tags
                if(tmp_3.length == 1) {
                    if (result[`:${tmp[0]}:`]['others'] === undefined || result[`:${tmp[0]}:`]['others'] === null){
                        result[`:${tmp[0]}:`]['others'] = [];
                    }
                    result[`:${tmp[0]}:`].others = result[`:${tmp[0]}:`].others.concat(tmp_3[0].split('/'));
                }
                else if (tmp_3[0] == 'ADDR') {
                    result[`:${tmp[0]}:`][tmp_3[0]+"//"] = tmp_3.slice(1,tmp_3.length)[0] ?? '';
                }
                //With sub-tags
                else {
                    if (tmp_3[1].split('/').length > 1) {
                        result[`:${tmp[0]}:`][tmp_3[0]+'//'] = tmp_3[1].split('/');
                    }
                    else {
                        result[`:${tmp[0]}:`][tmp_3[0]+'//'] = tmp_3[1] ?? '';
                    }
                }
            }
        }
        else {
            let tmp_other = tmp.slice(1,tmp.length).join(':').split('\n');
            let tmp_items = {};
            if (result[`:${tmp[0]}:`] === undefined || result[`:${tmp[0]}:`] === null) {
                result[`:${tmp[0]}:`] = {};
            }
            for (let j = 0; j < tmp_other.length; j++) {
                tmp_other[j] = tmp_other[j].split(':');
                tmp_items[tmp_other[j][0]] = tmp_other[j][1]
            }
            //Item at 0 has more than 2 item in it, then not header, using other as header
            if(tmp_other[0].length > 1) {
                result[`:${tmp[0]}:`]['others'] = tmp_items;
            }
            else {
                result[`:${tmp[0]}:`][tmp_other[0]] = tmp_items;
            }
        }
    }
    return result;
}

/**
 * @param {logContent} log_content
 * @param {boolean} is_db_log
 */
async function log(log_content,is_db_log=true) {
    let now = new Date();
    let log_str = `[${await dateFormat(now,'yyyy-mm-dd HH:MM:ss')}][${log_content.log_type}] ${log_content.content}`;

    //Ignore debug logs
    if((setting.debug_log ?? false) != true && log_content.log_type == 'DEBUG') {
        return;
    }

    console.log(log_str);
    FSPromises.appendFile(PATH+LOG_FILE,log_str+'\n');

    if (is_db_log == true) {
        try {
            if (db.connected == false){
                await db.connect();
            }
            db.query(`INSERT INTO [LOG] ([INSERT_TIME],[LOG_TYPE],[CONTENT]) VALUES ('${await dateFormat(now,"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'")}','${(log_content.log_type ?? '').replaceAll("'","")}','${(log_content.content.toString() ?? '').replaceAll("'","")}')`)
        }
        catch (error) {
            console.log(`[${await dateFormat(now,'yyyy-mm-dd HH:MM:ss')}][CRITICAL] ${error}`);
            console.log(`[${await dateFormat(now,'yyyy-mm-dd HH:MM:ss')}][CRITICAL] No database connected!!!!`);
        }
    }
}
async function getSequence() {
    let sequence = '0001';
    try {
        if(db.connected == false) {
            await db.connect();
        }
        let respond = await db.query(`SELECT TOP 1 [REQUEST_ID] FROM [REQUEST] WHERE CAST([REQUEST_CREATED_AT] AS DATE) = '${dateFormat(new Date(), 'yyyy-mm-dd')}' ORDER BY [REQUEST_CREATED_AT] DESC`);
        if (respond.rowsAffected > 0){
            sequence = "0000" + (parseInt(respond.recordset[0].REQUEST_ID.toString().substring(10,14))+1);
            sequence = sequence.substring(sequence.length-4);
        }
        else {
            log({log_type: 'INFO', content: 'First request of date, sequence = 1'}, true);
        }
    }
    catch (error) {
        log({log_type: 'CRITICAL', content: error}, true);
    }
    return sequence;
}
async function preInitCheck() {
    //Check TMP folder
    try {
        await FSPromises.access(PATH+TMP_RECEIVE_FORDER);
    }
    catch (error) {
        log({log_type: 'WARNING', content: error + '\nCreate ' + PATH+TMP_RECEIVE_FORDER},false);
        await FSPromises.mkdir(PATH+TMP_RECEIVE_FORDER)
    }

    try{
        await FSPromises.access(PATH+REQUEST_FILE_FOLDER);
    }
    catch (error) {
        log({log_type: 'WARNING', content: error + '\nCreate ' + PATH+REQUEST_FILE_FOLDER},false);
        await FSPromises.mkdir(PATH+REQUEST_FILE_FOLDER)
    }
}
async function postInitCheck() {

}
/**
 * 
 * @returns {result}
 */
async function loadConfiguration() {
    let result = {
        status: '',
        description: ''
    };
    let data = ''
    try {
        data = await FSPromises.readFile(PATH+CONFIGURATION_FILE);
        data = JSON.parse(data.toString());
    }
    catch (error) {
        result.status = 'Failed';
        result.description = error + '\n=======> Loading configuration from defalt!!!';
        return result;
    }
    if (data.DB_CONNECTION !== undefined && data.DB_CONNECTION !== null) {
        setting = data.GENERAL;
    }
    else {
        result.description += '\nNo general setting found, loading default!';
    }
    if (data.DB_CONNECTION !== undefined && data.DB_CONNECTION !== null) {
        db_conn = data.DB_CONNECTION;
    }
    else {
        result.description += '\nNo database connection found, loading default!';
    }
    if (data.SFTP_CONNECTION !== undefined && data.SFTP_CONNECTION !== null) {
        sftp_conn = data.SFTP_CONNECTION;
    }
    else {
        result.description += '\nNo SFTP connection found, loading default!';
    }
    if (data.JOB_TIMER !== undefined && data.JOB_TIMER !== null) {
        job_timer = data.JOB_TIMER;
    }
    else {
        result.description += '\nNo SFTP connection found, loading default!';
    }
    result.description.replace('\n','');
    if (result.description === '') {
        result.status = 'Success';
    }
    else {
        result.status = 'Failed'
    }
    return result;
}
async function requestListener() {
    log({log_type: 'INFO', content: 'Request listener is running!'});
    while(flags.exit == false) {
        let template_file = await FSPromises.readdir(PATH+REQUEST_EXCEL);
        for (let i = 0; i < modules.length; i++) {
            const tmp = require(PATH+MODULES_PATH+modules[i]);
            for (let j = 0; j < template_file.length; j++) {
                if (tmp.EXCEL === template_file[j]){
                    let excel_file = await XLSX.readFile(PATH+REQUEST_EXCEL+tmp.EXCEL);
                    let data = await XLSX.utils.sheet_to_json(excel_file.Sheets[excel_file.SheetNames[0]]);
                    for (let u = 0; u < data.length; u++) {
                        data[u]["sequence"] = await getSequence();

                        //Get additional data
                        let additional_data = await tmp.getSqlAdditionalData(data[u]);
                        if (additional_data.status == 'Success' && (additional_data.status ?? '') != '') {
                            try {
                                if(db.connected == false) {
                                    db.connect();
                                }
                                additional_data = await db.query(additional_data.content);
                            }
                            catch (error) {
                                log({log_type: 'CRITICAL', content: error}, true);
                            }
                            if((additional_data.recordset ?? '') != '') {
                                for (let t = 0; t < Object.keys(additional_data.recordset[0]).length; t++) {
                                    data[u][Object.keys(additional_data.recordset[0])[t].toLowerCase()] = additional_data.recordset[0][Object.keys(additional_data.recordset[0])[t]]
                                }
                            }
                        }

                        let tmp_result = await tmp.toUnencodedDataFin(data[u]);
                        try {
                            if(db.connected == false) {
                                db.connect();
                            }
                            let sql_str = `INSERT INTO [REQUEST] ([REQUEST_ID],[REQUEST_TYPE],[REQUEST_FILE_NAME],[CONTENT],[SQL_CMD],[REQUEST_STATUS],[REQUEST_STATE],[REQUEST_CREATED_AT],[UPDATED_AT]) VALUES ('${tmp_result.request_id}','${modules[i].replaceAll('.js','').toUpperCase()}','${tmp_result.file_name}','${await telexDeEnCode(tmp_result.content,false)}','${tmp_result.SQL_CMD.replaceAll(`'`,`''`)}','CREATED','CREATED',GETDATE(),GETDATE())`;
                            log({log_type: 'INFO', content: `Add ${modules[i].replaceAll('.js','').toUpperCase()} request ID ${tmp_result.request_id} into process flow`})
                            await db.query(sql_str);
                        }
                        catch (error) {
                            log({log_type: 'CRITICAL', content: error}, true);
                        }
                    }
                    await FSPromises.rm(PATH+REQUEST_EXCEL+tmp.EXCEL);
                }
            }
        }
        await sleep(job_timer.request_listener);
    }
}
async function requestSender() {
    log({log_type: 'INFO', content: 'Sender is running!'});
    while(flags.exit == false) {
        let today = new Date();
        pulse.sender = dateFormat(today,PULSE_DATE_TIME_FORMAT)
        let data;
        let fin_files = [];
        //Get request data content from database (Only get request data with has CREATED REQUEST_STATE)
        let query_string = `
            SELECT
                [REQUEST_ID],
                [REQUEST_TYPE], 
                [REQUEST_FILE_NAME],
                [CONTENT],
                [REQUEST_STATUS],
                [REQUEST_STATE],
                [REQUEST_CREATED_AT],
                [UPDATED_AT]
            FROM
                [REQUEST]
            WHERE
                [REQUEST_STATUS] = 'CREATED'
                AND [REQUEST_STATE] = 'CREATED'
        `;
        try {
            if (db.connected == false){
                await db.connect();
            }
            data = await db.query(query_string);
            pulse.sender = dateFormat(new Date(),PULSE_DATE_TIME_FORMAT);
        }
        catch (error) {
            log({log_type: 'CRITICAL', content: error},true);
        }
        for(let i = 0; i < data.recordset.length; i++) {
            try {
                //Create fin file from database content
                fin_files.push(PATH+REQUEST_FILE_FOLDER+data.recordset[i].REQUEST_FILE_NAME);
                await FSPromises.writeFile(PATH+REQUEST_FILE_FOLDER+data.recordset[i].REQUEST_FILE_NAME,data.recordset[i].CONTENT);
                log({log_type: 'INFO', content: `Request file ${data.recordset[i].REQUEST_FILE_NAME} created!`},true);
                //Put request file into fin data folder and remove file from tmp folder
                switch (setting.fin_file_remote_mode) {
                    case 'SFTP':
                        //Put request file into SFTP server
                        let sftp = new SFTPClient();
                        await sftp.connect(sftp_conn);
                        await sftp.put(PATH+REQUEST_FILE_FOLDER+data.recordset[i].REQUEST_FILE_NAME,(setting.fin_file_remote_root_path ?? REMOTE_ROOT_PATH)+REMOTE_SEND_FOLDER+data.recordset[i].REQUEST_FILE_NAME);
                        sftp.end();
                        break;
                    case 'LOCAL':
                        //Put request file into fin folder data in local
                        await FSPromises.copyFile(PATH+REQUEST_FILE_FOLDER+data.recordset[i].REQUEST_FILE_NAME,(setting.fin_file_remote_root_path ?? REMOTE_ROOT_PATH)+REMOTE_SEND_FOLDER+data.recordset[i].REQUEST_FILE_NAME);
                        break;
                }

                await FSPromises.rm(PATH+REQUEST_FILE_FOLDER+data.recordset[i].REQUEST_FILE_NAME);
                log({log_type: 'INFO', content: `Request file ${data.recordset[i].REQUEST_FILE_NAME} sent to ${(setting.fin_file_remote_root_path ?? REMOTE_ROOT_PATH)+REMOTE_SEND_FOLDER+data.recordset[i].REQUEST_FILE_NAME}!`},true);
                //Change request status
                if (db.connected == false){
                    await db.connect();
                }
                db.query(`UPDATE [REQUEST] SET [UPDATED_AT] = GETDATE(), [REQUEST_STATE] = 'REQUESTED' WHERE [REQUEST_ID] = '${data.recordset[i].REQUEST_ID}'`);
                log({log_type: 'INFO', content: `Change REQUEST_STATE of REQUEST_ID ${data.recordset[i].REQUEST_ID} to REQUESTED!`},true)
            }
            catch (error) {
                log({log_type: 'CRITICAL', content: error},true);
            }
        }
        await sleep(job_timer.request_sender);
    }
}
async function loadModules() {
    modules = await FSPromises.readdir(PATH+MODULES_PATH);
    //Check module file
    for (let i = 0; i < modules.length; i++) {
        const tmp = require(PATH+MODULES_PATH+modules[i]);
        let self_check_result = await tmp.selfCheck();
        if(self_check_result.status == 'Success') {
            log({log_type: 'INFO', content: `Module ${modules[i]} loaded!`})
        }
        else {
            log({log_type: 'WARNING', content: `Module ${modules[i]} load failed with error \n${self_check_result.description}`})
        }
    }
}
async function receiverListener() {
    while (flags.exit != true){
        const nak_str = `{451:1}`;
        const ack_str = `{451:0}`;
        const done_str = `:20:`;
        let sftp_client = new SFTPClient();
        let receive_files = [];
        let promises = [];
        let requests;
        let request_done;
        let receive_folder;
        let nak_ack_query_str = `
            SELECT
                [REQUEST_ID]
            FROM
                [REQUEST]
            WHERE
                [REQUEST_STATUS] = 'CREATED'
                AND [REQUEST_STATE] = 'REQUESTED'
        `;
        let done_query_str = `
        SELECT
            [REQUEST_ID],
            [SQL_CMD]
        FROM
            [REQUEST]
        WHERE
            ([REQUEST_STATUS] = 'ACCEPTED' OR [REQUEST_STATUS] = 'DONE')
            AND ([REQUEST_STATE] = 'RECEIVED' OR [REQUEST_STATE] = 'DONE')
        `;
        switch (setting.fin_file_remote_mode){
            case 'SFTP':
                //Download fin file from remote server
                try {
                    await sftp_client.connect(sftp_conn);
                    log({log_type: 'INFO', content: 'Getting files list from SFTP server'});
                    receive_files = await sftp_client.list((setting.fin_file_remote_root_path ?? REMOTE_ROOT_PATH)+REMOTE_RECEIVE_FOLDER);
                    log({log_type: 'INFO', content: 'Files list received'});
                    log({log_type: 'INFO', content: 'Downloading data from SFTP server'});
                    for (let i = 0; i < receive_files.length; i++){
                        let parallels = [];
                        if(receive_files[i].name.includes('.fin')){
                            parallels.push(await sftp_client.get((setting.fin_file_remote_root_path ?? REMOTE_ROOT_PATH)+REMOTE_RECEIVE_FOLDER+receive_files[i].name,PATH+TMP_RECEIVE_FORDER+receive_files[i].name));
                        }
                        if (parallels.length >= 10) {
                            await Promise.all(parallels);
                            parallels = [];
                        }
                    }
                    log({log_type: 'INFO', content: 'Downloaded data from SFTP server'});
                    sftp_client.end();
                }
                catch (error) {
                    log({log_type: 'CRITICAL', content: error}, true);
                    sftp_client.end();
                    await sleep(job_timer.receiver_listener);
                    continue;
                }
                //Build receive folder location
                receive_folder = PATH+TMP_RECEIVE_FORDER;
                break;
            case 'LOCAL':
                let tmp = await FSPromises.readdir(setting.fin_file_remote_root_path+REMOTE_RECEIVE_FOLDER);
                for (let i = 0; i < tmp.length; i++) {
                    try {
                        if ((await FSPromises.stat(setting.fin_file_remote_root_path+REMOTE_RECEIVE_FOLDER + tmp[i])).isFile() == true) {
                            receive_files.push({ name: tmp[i] });
                        }
                    }
                    catch (error) {
                        console.log(error);
                        await sleep(job_timer.receiver_listener);
                    }
                }
                //Build receive folder location
                receive_folder = setting.fin_file_remote_root_path+REMOTE_RECEIVE_FOLDER;
                break;
        }

        //Get nak/ack data from DB
        try {
            if(db.connected == false){
                db.connect();
            }
            requests = await db.query(nak_ack_query_str);
            request_done = await db.query(done_query_str);
        }
        catch (error) {
            log({log_type: 'CRITICAL', content: error}, true);
        }

        //Update ACK file
        if (requests.recordset.length > 0) {
            for (let i = 0; i < receive_files.length; i++) {
                //Ignore unexpected items of folder
                if (receive_files[i].name === undefined || receive_files[i].name === null) {
                    log({log_type: 'DEBUG', content: `Ignore item: \n${receive_files[i]}`});
                    continue;
                }
                if (receive_files[i].name.toString().includes('.fin') === true) {
                    let tmp_file;
                    try {
                        await FSPromises.access(receive_folder+receive_files[i].name);
                        tmp_file = await FSPromises.readFile(receive_folder+receive_files[i].name);
                    }
                    catch (error) {
                        log({log_type: 'WARNING', content: `File ${receive_files[i]}\n${error}`});
                        continue;
                    }
                    for (let j = 0; j < requests.recordset.length; j++) {
                        if (tmp_file.toString().includes(requests.recordset[j].REQUEST_ID) === true) {
                            if (tmp_file.toString().includes(ack_str)) {
                                log({log_type: 'INFO', content: `Updating ACK file ${receive_files[i].name} to request ID ${requests.recordset[j].REQUEST_ID}`});
                                try {
                                    if (db.connected === true) {
                                        db.connect();   
                                    }
                                    await db.query(`UPDATE [REQUEST] SET [ACK_FILE_NAME] = '${receive_files[i].name}', [ACK_CONTENT] = '${tmp_file.toString()}', [ACK_STATUS] = 'RECEIVED',[REQUEST_STATE] = 'RECEIVED', [REQUEST_STATUS] = 'ACCEPTED' WHERE [REQUEST_ID] = '${requests.recordset[j].REQUEST_ID}'`);
                                }
                                catch (error) {
                                    log({log_type: 'CRITICAL', content: error},true);
                                }
                            }
                            if (tmp_file.toString().includes(nak_str)) {
                                log({log_type: 'INFO', content: `Update NAK file ${receive_files[i].name} to request ID ${requests.recordset[j].REQUEST_ID}`});
                                try {
                                    if (db.connected === true) {
                                        db.connect();   
                                    }
                                    await db.query(`UPDATE [REQUEST] SET [NAK_FILE_NAME] = '${receive_files[i].name}', [NAK_CONTENT] = '${tmp_file.toString()}', [NAK_STATUS] = 'RECEIVED', [REQUEST_STATUS] = 'REJECTED', [REQUEST_STATE] = 'COMPLETED' WHERE [REQUEST_ID] = '${requests.recordset[j].REQUEST_ID}'`);
                                }
                                catch (error) {
                                    log({log_type: 'CRITICAL', content: error},true);
                                }
                            }
                        }
                    }
                }
            }
        }

        //Update DONE file
        if (request_done.recordset.length > 0) {
            for (let i = 0; i < receive_files.length; i++) {
                //Ignore unexpected items of folder
                if (receive_files[i].name === undefined || receive_files[i].name === null) {
                    log({log_type: 'DEBUG', content: `Ignore item: \n${receive_files[i]}`});
                    continue;
                }
                if (receive_files[i].name.toString().includes('.fin') === true) {
                    let tmp_file;
                    try {
                        await FSPromises.access(receive_folder+receive_files[i].name);
                        tmp_file = await FSPromises.readFile(receive_folder+receive_files[i].name);
                    }
                    catch (error) {
                        log({log_type: 'WARNING', content: `File ${receive_files[i]}\n${error}`});
                        continue;
                    }
                    for (let j = 0; j < request_done.recordset.length; j++) {
                        if (tmp_file.toString().includes(request_done.recordset[j].REQUEST_ID) === true) {
                            if (tmp_file.toString().includes(done_str)) {
                                log({log_type: 'INFO', content: `Updating DONE file ${receive_files[i].name} to request ID ${request_done.recordset[j].REQUEST_ID}`});
                                try {
                                    if (db.connected === true) {
                                        db.connect();   
                                    }
                                    await db.query(`UPDATE [REQUEST] SET [DONE_FILE_NAME] = '${receive_files[i].name}', [DONE_CONTENT] = '${tmp_file.toString()}', [DONE_STATUS] = 'RECEIVED', [REQUEST_STATE] = 'DONE', [REQUEST_STATUS] = 'DONE' WHERE [REQUEST_ID] = '${request_done.recordset[j].REQUEST_ID}'`);
                                    log({log_type: 'INFO', content: `Updated DONE file ${receive_files[i].name} to request ID ${request_done.recordset[j].REQUEST_ID}`});
                                    //Step 2: Execute SQL_CMD
                                    log({log_type: 'INFO', content: `Execute SQL_CMD for request ID ${request_done.recordset[j].REQUEST_ID}`});
                                    if (request_done[j].SQL_CMD !== undefined && request_done[j].SQL_CMD !== null){
                                        await db.query(request_done[j].SQL_CMD);
                                    }
                                    log({log_type: 'INFO', content: `Executed SQL_CMD for request ID ${request_done.recordset[j].REQUEST_ID}`});
                                    log({log_type: 'INFO', content: `Finishing request ID ${request_done.recordset[j].REQUEST_ID}`});
                                    await db.query(`UPDATE [REQUEST] SET [REQUEST_STATE] = 'COMPLETED' WHERE [REQUEST_ID] = '${request_done.recordset[j].REQUEST_ID}'`);
                                    log({log_type: 'INFO', content: `Request ID ${request_done.recordset[j].REQUEST_ID} process finished!!!!`});
                                }
                                catch (error) {
                                    log({log_type: 'CRITICAL', content: error},true);
                                }
                            }
                        }
                    }
                }
            }
        }
        if(setting.fin_file_remote_mode !== 'LOCAL' && setting.fin_file_remote_mode !== '' && setting.fin_file_remote_mode !== null && setting.fin_file_remote_mode !== undefined) {
            try {
                await FSPromises.rm(PATH+TMP_RECEIVE_FORDER,{ recursive: true, force: true });
                await FSPromises.mkdir(PATH+TMP_RECEIVE_FORDER);
            }
            catch (error) {
                log({log_type: 'CRITICAL', content: error},true);
            }
        }
        await sleep(job_timer.receiver_listener);
    }

}
async function pulseCheck() {
    log({log_type: 'INFO', content: 'PulseCheck is running!'});
    while(flags.exit == false) {
        let now = new Date();
        for (let i = 0; i < Object.keys(pulse).length; i++) {
            if (now - Date(pulse[Object.keys(pulse)[i]]) > 30000){
                log({log_type: 'CRITICAL',  content: `Pulse check, ${pulse[Object.keys(pulse)[i]]} has no respond for ${now - Date(pulse[Object.keys(pulse)[i]])} ms!!!`})
            }
        }
        await sleep(job_timer.request_sender);
    }
}
/**
 * @param {number} exit_code
 * App failed exit_code=1
 * App run find but need to be exit exit_code=0
 * Default exit_code=1 
 */
async function exit(exit_code=1) {
    await log({log_type: 'INFO', content: 'Start clean up process'});
    /** Clean process define here */

    
    /** End clean process */
    //End DB connection
    await log({log_type: 'INFO', content: 'Done cleaning up, close connection to database and exit!'});
    /*if (db.connected == true){
        try {
            await db.close();
        }
        catch (error) {
            await log({log_type: 'INFO', content: error}, false);
        }    
    }*/
    await log({log_type: 'INFO', content: 'Done!!!! Exit, good bye!!!!'}, false);
    //Exit code 1
    process.exit(exit_code);
}
/**
 * Initial function, will run through all the time.
 * 
 */
async function initial() {
    //Load configuration
    let tmp_result = await loadConfiguration();
    if (tmp_result.status == 'Success') {
        flags.configuration_load = true;
    }
    else {
        await log({log_type: 'WARNING', content: tmp_result.description},false);
    }
    log({log_type: 'INFO', content: 'Loaded configuration!'},false);

    //Init database connection
    try {
        db = new MSSQL.ConnectionPool(db_conn);
        await db.connect();
        flags.database_connected = true;
        log({log_type: 'INFO', content: 'Success connected to database'},false);
    }
    catch (error) {
        await log({log_type: 'WARNING', content : error},false);
    }

    //Loading fin file location
    switch (setting.fin_file_remote_mode) {
        case 'SFTP':
            //Init SFTP connection
            try {
                let sftp = new SFTPClient();
                await sftp.connect(sftp_conn);
                log({log_type: 'INFO', content: 'Success connected to SFTP server'},false);
                sftp.end();
            }
            catch (error) {
                log({log_type: 'CRITICAL', content: error});
            }
            break;
        case 'LOCAL':
            try {
                await FSPromises.access(setting.fin_file_remote_root_path);
            }
            catch (error) {
                log({log_type: 'CRITICAL', content: error});
            }
            break;
        default:
            log({log_type: 'CRITICAL', content: 'Setting at fin_file_remote_root_path is not defined!!!'});
    }


    //Load fin processing modules
    await loadModules();


    /**
     * Repeatable jobs init here
     */
    //Start Request Listener
    requestListener();

    //Start Request sender
    requestSender();

    //Start Receive listenner
    receiverListener();

    //Start Pulse check job
    pulseCheck();
}

/** Process control */
process.on('SIGINT',function() {exit(0)});
process.on('exit',function() {exit(0)});

/** Main function */
async function main() {
    //Step 1 Pre-initial check
    await preInitCheck();

    //Step 2 Initial
    initial();
}
main();