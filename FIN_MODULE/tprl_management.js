const dateFormat = require('dateformat');
const FSPromises = require('fs/promises');

//Template variables
const VAR_TODAY_YYMMDD = '$(today_yymmdd)$';
const VAR_TODAY_YYYYMMDD = '$(today_yyyymmdd)$';
const VAR_SEQUENCE = '$(sequence)$';
const VAR_ACCOUNT_ID = '$(account_id)$';
const VAR_ID_TYPE = '$(id_type)$';
const VAR_NATIONALITY_CODE = '$(nationality_code)$';
const VAR_ID_NO = '$(id_no)$';
const VAR_DATE_OF_BIRTH = '$(date_of_birth_yyyymmdd)$';
const VAR_OWNER_ISSUE_PLACE = '$(owner_issue_place)$';
const VAR_OWNER_ISSUE_DATE = '$(owner_issue_date_yyyymmdd)$';
const VAR_TPRL_TYPE_CODE = '$(tprl_type_code)$';
const VAR_QUANTITY = '$(quantity)$';
const VAR_TPRL_CODE = '$(tprl_code)$';
const VAR_ACCOUNT_NAME = '$(account_name)%'

//Constant
const PATH = './';
const TEMPLATE_FOLDER = 'FIN_TEMPLATE/';
const TEMPLATE_EXCEL = 'REQUEST_EXCEL/'
const TEMPLATE_FILE_NAME = 'TPRL_MANAGEMENT.TEMPLATE';
const EXCEL_TEMPLATE = 'TPRL_MANAGEMENT_TEMPLATE.xlsx';
const EXCEL = 'TPRL_MANAGEMENT.xlsx';
const SQL_ADDITIONAL_DATA_DB = `
    SELECT
        [IDTYPE],
        [NATIONALITYCODE],
        [ACCOUNTNAME],
        [IDNO]
    FROM
        [ACCOUNT]
    WHERE
        [ACCOUNTID] = '$(account_id)$'
`;
const SQL_CMD_TEMPLATE = `
INSERT INTO [ACCOUNT] (
    [ACCOUNTID],
    [TPRL_TYPE],
    [TPRL_CODE],
    [QUANTITY],
    [ACCOUNTING_DATE],
    [OWNER_ISSUE_PLACE],
    [OWNER_ISSUE_DATE],
    [CREATE_AT]
)
VALUES
('$(account_id)$','$(tprl_type_code)$','$(tprl_code)$','$(quantity)$','$(today_yyyymmdd)$','$(owner_issue_place)$','$(owner_issue_date_yyyymmdd)$',GETDATE())
`;

/**
 * Doc for data for fin files
 * @typedef {Object} FinData
 * @property {string} sequence
 * @property {string} account_id
 * @property {string} accountname
 * @property {string} id_type
 * @property {string} nationality_code
 * @property {string} id_no
 * @property {Date} date_of_birth
 * @property {string} owner_issue_place
 * @property {Date} owner_issue_date
 * @property {int} quantity
 */

async function selfCheck() {
    let result = {
        status: '',
        description: '',
    }
    try {
        await FSPromises.access(PATH+TEMPLATE_FOLDER+TEMPLATE_FILE_NAME);
        await FSPromises.access(PATH+TEMPLATE_EXCEL+EXCEL_TEMPLATE);
        result.status = 'Success';
    }
    catch (error) {
        result.status = 'Failed';
        result.description = error;
    }
    return result;
}

/**
 * @returns {json}
 */
async function getTemplateFinFile() {
    let result = {
        status: '',
        content: '',
        description: ''
    };

    try {
        let data = await FSPromises.readFile(PATH+TEMPLATE_FOLDER+TEMPLATE_FILE_NAME);
        result.status = 'Success';
        result.content = data.toString();
    }
    catch (error) {
        result.status = 'Failed';
        result.description = error;
    }
    return result;
}


async function getSqlAdditionalData(data) {
    let result = {
        status: '',
        description: '',
        content: ''
    };
    
    result.content = SQL_ADDITIONAL_DATA_DB.replaceAll(VAR_ACCOUNT_ID,data.account_id ?? '')
    result.status = 'Success';
    return result;
}

/**
 * 
 * @param {json} content
 * @returns {json} 
 */
/*async function fromDataFin(content) {
    let data = Object.assign(content);
    let json_data = {
        sequence: '',
        account_id: '',
        account_name: '',
        id_type: '',
        nationality_code: '',
        id_no: '',
        id_issue_date: '',
        id_issue_place: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        shareholder_type: '',
        field_of_activity: '',
        bussiness_type: '',
        professional_registration_criteria: '',
        PINV: '',
        profesional_date_start: '',
        profesional_date_end: ''
    }
    
}*/

/**
 * 
 * @param {string} template_content 
 * @param {FinData} data
 * @returns {json}
 */
async function toUnencodedDataFin(data) {
    let finFile = await getTemplateFinFile();
    let today = Date();
    let result = {
        status: '',
        description: '',
        request_id: 'ACBS'+dateFormat(today,'yymmdd')+data.sequence.toString(),
        file_name: dateFormat(today,'yyyymmdd HHMMss') + ' ' + data.sequence.toString() + '.fin',
        SQL_CMD: Object.assign(SQL_CMD_TEMPLATE),
        content: ''
    };

    //Check data before generate
    if ((data.sequence ?? '') == '') {
        result.status = 'Failed';
        result.description = 'No sequence!!!';
        return result;
    }

    //Start generate content
    finFile.content = finFile.content.replaceAll(VAR_TODAY_YYMMDD,dateFormat(today,'yymmdd'))
    finFile.content = finFile.content.replaceAll(VAR_TODAY_YYYYMMDD,dateFormat(today,'yyyymmdd'))
    finFile.content = finFile.content.replaceAll(VAR_SEQUENCE,data.sequence);
    finFile.content = finFile.content.replaceAll(VAR_ACCOUNT_ID,data.account_id);
    finFile.content = finFile.content.replaceAll(VAR_ACCOUNT_NAME,data.accountname ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ID_TYPE,data.idtype ?? '');
    finFile.content = finFile.content.replaceAll(VAR_NATIONALITY_CODE,data.nationalitycode ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ID_NO,data.idno ?? '');
    finFile.content = finFile.content.replaceAll(VAR_DATE_OF_BIRTH,dateFormat(data.date_of_birth,'yyyymmdd') ?? '');
    finFile.content = finFile.content.replaceAll(VAR_OWNER_ISSUE_PLACE,data.owner_issue_place ?? '');
    finFile.content = finFile.content.replaceAll(VAR_OWNER_ISSUE_DATE,dateFormat(data.owner_issue_date,'yyyymmdd') ?? '');
    finFile.content = finFile.content.replaceAll(VAR_TPRL_TYPE_CODE,data.tprl_type_code ?? '');
    finFile.content = finFile.content.replaceAll(VAR_QUANTITY,data.quantity ?? '');
    finFile.content = finFile.content.replaceAll(VAR_TPRL_CODE,data.tprl_code ?? '');

    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TODAY_YYMMDD,dateFormat(today,'yymmdd'))
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TODAY_YYYYMMDD,dateFormat(today,'yyyymmdd'))
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_SEQUENCE,data.sequence);
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ACCOUNT_ID,data.account_id);
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ACCOUNT_NAME,data.accountname ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ID_TYPE,data.idtype ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_NATIONALITY_CODE,data.nationalitycode ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ID_NO,data.idno ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_DATE_OF_BIRTH,dateFormat(data.date_of_birth,'yyyymmdd') ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_OWNER_ISSUE_PLACE,data.owner_issue_place ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_OWNER_ISSUE_DATE,dateFormat(data.owner_issue_date,'yyyymmdd') ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TPRL_TYPE_CODE,data.tprl_type_code ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_QUANTITY,data.quantity ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TPRL_CODE,data.tprl_code ?? '');

    result.status = 'Success';
    result.content = finFile.content;

    //Return generated data
    return result;
}

async function test() {
    let data  = {
        sequence: '0004',
        account_id: '006C111111',
        idtype: '001',
        nationalitycode: 'VN',
        idno: '025564123589',
        date_of_birth: '1997-02-06',
        owner_issue_place: 'Cục CSQL',
        owner_issue_date: '2022-01-02',
        tprl_type_code: '1',
        tprl_code: 'AAAR42202',
        quantity: 10000
    };

    let fin = await toUnencodedDataFin(data);

    const encode = (content,decode_mode) => {
        let encode_decode_table = {
            "đ":"?dd?",
            "ă":"?aw?",
            "â":"?aa?",
            "ê":"?ee?",
            "ô":"?oo?",
            "ơ":"?ow?",
            "ư":"?uw?",
            "á":"?as?",
            "ắ":"?aws?",
            "ấ":"?aas?",
            "e":"?es?",
            "ế":"?ees?",
            "ố":"?oos?",
            "ớ":"?ows?",
            "ú":"?us?",
            "ứ":"?uws?",
            "í":"?is?",
            "à":"?af?",
            "ằ":"?awf?",
            "ầ":"?aaf?",
            "è":"?ef?",
            "ề":"?eef?",
            "ò":"?of?",
            "ồ":"?oof?",
            "ờ":"?owf?",
            "ù":"?uf?",
            "ừ":"?uwf?",
            "ì":"?if?",
            "ả":"?ar?",
            "ẳ":"?awr?",
            "ẩ":"?aar?",
            "ẻ":"?er?",
            "ể":"?eer?",
            "ỏ":"?or?",
            "ổ":"?oor?",
            "ở":"?owr?",
            "ủ":"?ur?",
            "ử":"?uwr?",
            "ỉ":"?ir?",
            "ã":"?ax?",
            "ẵ":"?awx?",
            "ẫ":"?aax?",
            "ẽ":"?ex?",
            "ễ":"?eex?",
            "õ":"?ox?",
            "ỗ":"?oox?",
            "ỡ":"?owx?",
            "ũ":"?ux?",
            "ữ":"?uwx?",
            "ĩ":"?ix?",
            "ạ":"?aj?",
            "ặ":"?awj?",
            "ậ":"?aaj?",
            "ẹ":"?ej?",
            "ệ":"?eej?",
            "ọ":"?oj?",
            "ộ":"?ooj?",
            "ợ":"?owj?",
            "ụ":"?uj?",
            "ự":"?uwj?",
            "ị":"?ij?",
            "@":"(at)",
            "Đ":"?DD?",
            "Ă":"?AW?",
            "Â":"?AA?",
            "Ê":"?EE?",
            "Ô":"?OO?",
            "Ơ":"?OW?",
            "Ư":"?UW?",
            "Á":"?AS?",
            "Ắ":"?AWS?",
            "Ấ":"?AAS?",
            "É":"?ES?",
            "Ế":"?EES?",
            "Ố":"?OOS?",
            "Ớ":"?OWS?",
            "Ú":"?US?",
            "Ứ":"?UWS?",
            "Í":"?IS?",
            "À":"?AF?",
            "Ằ":"?AWF?",
            "Ầ":"?AAF?",
            "È":"?EF?",
            "Ề":"?EEF?",
            "Ò":"?OF?",
            "Ồ":"?OOF?",
            "Ờ":"?OWF?",
            "Ù":"?UF?",
            "Ừ":"?UWF?",
            "Ì":"?IF?",
            "Ả":"?AR?",
            "Ẳ":"?AWR?",
            "Ẩ":"?AAR?",
            "Ẻ":"?ER?",
            "Ể":"?EER?",
            "Ỏ":"?OR?",
            "Ổ":"?OOR?",
            "Ở":"?OWR?",
            "Ủ":"?UR?",
            "Ử":"?UWR?",
            "Ỉ":"?IR?",
            "Ã":"?AX?",
            "Ẵ":"?AWX?",
            "Ẫ":"?AAX?",
            "Ẽ":"?EX?",
            "Ễ":"?EEX?",
            "Õ":"?OX?",
            "Ỗ":"?OOX?",
            "Ỡ":"?OWX?",
            "Ũ":"?UX?",
            "Ữ":"?UWX?",
            "Ĩ":"?IX?",
            "Ạ":"?AJ?",
            "Ặ":"?AWJ?",
            "Ậ":"?AAJ?",
            "Ẹ":"?EJ?",
            "Ệ":"?EEJ?",
            "Ọ":"?OJ?",
            "Ộ":"?OOJ?",
            "Ợ":"?OWJ?",
            "Ụ":"?UJ?",
            "Ự":"?UWJ?",
            "Ị":"?IJ?"        
        }
        for(let i = 0; i < Object.keys(encode_decode_table).length; i++) {
            if (decode_mode == true) {
                if (content.includes(encode_decode_table[Object.keys(encode_decode_table)[i]]) == true) {
                    content = content.replaceAll(encode_decode_table[Object.keys(encode_decode_table)[i]],Object.keys(encode_decode_table)[i]);
                }
            }
            else {
                if (content.includes(Object.keys(encode_decode_table)[i]) == true) {
                    content = content.replaceAll(Object.keys(encode_decode_table)[i],encode_decode_table[Object.keys(encode_decode_table)[i]]);
                }
            }
        }
        return content;
      };
    
    fin.content = encode(fin.content,false);
}
test();

module.exports = {
    /*fromDataFin,*/
    toUnencodedDataFin,
    selfCheck,
    getSqlAdditionalData,
    EXCEL_TEMPLATE,
    TEMPLATE_FILE_NAME,
    EXCEL
}