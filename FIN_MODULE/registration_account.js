const dateFormat = require('dateformat');
const FSPromises = require('fs/promises');

//Template variables
const VAR_TODAY_YYMMDD = '$(today_yymmdd)$';
const VAR_TODAY_YYYYMMDD = '$(today_yyyymmdd)$';
const VAR_SEQUENCE = '$(sequence)$';
const VAR_ACOUNT_ID = '$(account_id)$';
const VAR_ACCOUNT_NAME = '$(account_name)$';
const VAR_ID_TYPE = '$(id_type)$';
const VAR_NATIONALITY_CODE = '$(nationality_code)$';
const VAR_ID_NO = '$(id_no)$';
const VAR_ISSUE_DATE = '$(id_issue_date)$';
const VAR_ID_ISSUE_PLACE = '$(id_issue_place)$'
const VAR_EMAIL = '$(email)$';
const VAR_PHONE = '$(phone)$';
const VAR_ADDRESS = '$(address)$';
const VAR_CITY = '$(city)$';
const VAR_SHAREHOLDER_TYPE = '$(shareholder_type)$';
const VAR_FIELD_OF_ACTIVITY = '$(field_of_activity)$';
const VAR_BUSSINESS_TYPE = '$(bussiness_type)$';
const VAR_PROFESSIONAL_REGISTRATION_CRITERIA = '$(professional_registration_criteria)$';
const VAR_PINV = '$(PINV)$';
const VAR_PROFESSIONAL_DATE_START = '$(profesional_date_start)$';
const VAR_PROFESSIONAL_DATE_END = '$(profesional_date_end)$';

//Constant
const PATH = './';
const TEMPLATE_FOLDER = 'FIN_TEMPLATE/';
const TEMPLATE_EXCEL = 'REQUEST_EXCEL/'
const TEMPLATE_FILE_NAME = 'REGISTRATION_ACCOUNT.TEMPLATE';
const EXCEL_TEMPLATE = 'REGISTRATION_ACCOUNT_TEMPLATE.xlsx';
const EXCEL = 'REGISTRATION_ACCOUNT.xlsx';
const SQL_CMD_TEMPLATE = `
INSERT INTO [ACCOUNT] (
    [ACCOUNTID],
    [ACCOUNTNAME],
    [IDTYPE],
    [NATIONALITYCODE],
    [IDNO],
    [ISSUEDATE],
    [IDISSUEPLACE],
    [EMAIL],
    [PHONE],
    [ADDRESS],
    [CITY],
    [SHAREHOLDERTYPE],
    [FIELDOFACTIVITY],
    [BUSSINESSTYPE],
    [PROFESSIONALREGISTRATIONCRITERIA],
    [PINV],
    [PROFESSIONALDATESTART],
    [PROFESSIONALDATEEND],
    [CHANGEDATETIME],
    [INSERTDATETIME]
)
VALUES
('$(account_id)$','$(account_id)$','$(id_type)$','$(nationality_code)$','$(id_no)$','$(id_issue_date)$','$(id_issue_place)$','$(email)$','$(phone)$','$(address)$','$(city)$','$(shareholder_type)$','$(field_of_activity)$','$(professional_registration_criteria)$','$(PINV)$','$(profesional_date_start)$','$(profesional_date_end)$',GETDATE(),GETDATE())
`;

/**
 * Doc for data for fin files
 * @typedef {Object} FinData
 * @property {string} sequence
 * @property {string} account_id
 * @property {string} account_name
 * @property {string} id_type
 * @property {string} nationality_code
 * @property {string} id_no
 * @property {Date} id_issue_date
 * @property {string} id_issue_place
 * @property {string} email
 * @property {string} phone
 * @property {string} address
 * @property {string} city
 * @property {String} shareholder_type
 * @property {string} field_of_activity
 * @property {string} bussiness_type
 * @property {string} professional_registration_criteria
 * @property {string} PINV
 * @property {Date} profesional_date_start
 * @property {Date} profesional_date_end
 */

async function selfCheck() {
    let result = {
        status: '',
        description: '',
    }
    try {
        await FSPromises.access(PATH+TEMPLATE_FOLDER+TEMPLATE_FILE_NAME);
        await FSPromises.access(PATH+TEMPLATE_EXCEL+EXCEL_TEMPLATE);
        result.status = 'Success';
    }
    catch (error) {
        result.status = 'Failed';
        result.description = error;
    }
    return result;
}

/**
 * @returns {json}
 */
async function getTemplateFinFile() {
    let result = {
        status: '',
        content: '',
        description: ''
    };

    try {
        let data = await FSPromises.readFile(PATH+TEMPLATE_FOLDER+TEMPLATE_FILE_NAME);
        result.status = 'Success';
        result.content = data.toString();
    }
    catch (error) {
        result.status = 'Failed';
        result.description = error;
    }
    return result;
}

async function getSqlAdditionalData(data) {
    let result = {
        status: '',
        description: '',
        content: ''
    };
    
    result.status = 'Success';
    return result;
}

/**
 * 
 * @param {json} content
 * @returns {json} 
 */
/*async function fromDataFin(content) {
    let data = Object.assign(content);
    let json_data = {
        sequence: '',
        account_id: '',
        account_name: '',
        id_type: '',
        nationality_code: '',
        id_no: '',
        id_issue_date: '',
        id_issue_place: '',
        email: '',
        phone: '',
        address: '',
        city: '',
        shareholder_type: '',
        field_of_activity: '',
        bussiness_type: '',
        professional_registration_criteria: '',
        PINV: '',
        profesional_date_start: '',
        profesional_date_end: ''
    }
    
}*/

/**
 * 
 * @param {string} template_content 
 * @param {FinData} data
 * @returns {json}
 */
async function toUnencodedDataFin(data) {
    let finFile = await getTemplateFinFile();
    let today = Date();
    let result = {
        status: '',
        description: '',
        request_id: 'ACBS'+dateFormat(today,'yymmdd')+data.sequence.toString(),
        file_name: dateFormat(today,'yyyymmdd HHMMss') + ' ' + data.sequence.toString() + '.fin',
        SQL_CMD: Object.assign(SQL_CMD_TEMPLATE),
        content: ''
    };

    //Check data before generate
    if ((data.sequence ?? '') == '') {
        result.status = 'Failed';
        result.description = 'No sequence!!!';
        return result;
    }

    //Start generate content
    finFile.content = finFile.content.replaceAll(VAR_TODAY_YYMMDD,dateFormat(today,'yymmdd') ?? '')
    finFile.content = finFile.content.replaceAll(VAR_TODAY_YYYYMMDD,dateFormat(today,'yyyymmdd') ?? '')
    finFile.content = finFile.content.replaceAll(VAR_SEQUENCE,data.sequence ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ACOUNT_ID,data.account_id ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ACCOUNT_NAME,data.account_name ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ID_TYPE,data.id_type ?? '');
    finFile.content = finFile.content.replaceAll(VAR_NATIONALITY_CODE,data.nationality_code ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ID_NO,data.id_no ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ISSUE_DATE,dateFormat(data.id_issue_date,'yyyymmdd') ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ID_ISSUE_PLACE,data.id_issue_place ?? '');
    finFile.content = finFile.content.replaceAll(VAR_EMAIL,data.email ?? '');
    finFile.content = finFile.content.replaceAll(VAR_PHONE,data.phone ?? '');
    finFile.content = finFile.content.replaceAll(VAR_ADDRESS,data.address ?? '');
    finFile.content = finFile.content.replaceAll(VAR_CITY,data.city ?? '');
    finFile.content = finFile.content.replaceAll(VAR_SHAREHOLDER_TYPE,data.shareholder_type ?? '');
    finFile.content = finFile.content.replaceAll(VAR_FIELD_OF_ACTIVITY,data.field_of_activity ?? '');
    finFile.content = finFile.content.replaceAll(VAR_BUSSINESS_TYPE,data.bussiness_type ?? '');
    finFile.content = finFile.content.replaceAll(VAR_PROFESSIONAL_REGISTRATION_CRITERIA,data.professional_registration_criteria ?? '');
    finFile.content = finFile.content.replaceAll(VAR_PINV,data.PINV ?? '');
    finFile.content = finFile.content.replaceAll(VAR_PROFESSIONAL_DATE_START,dateFormat(data.profesional_date_start,'yyyymmdd') ?? '');
    finFile.content = finFile.content.replaceAll(VAR_PROFESSIONAL_DATE_END,dateFormat(data.profesional_date_end,'yyyymmdd') ?? '');

    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TODAY_YYMMDD,dateFormat(today,'yy-mm-dd') ?? '')
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_TODAY_YYYYMMDD,dateFormat(today,'yyyy-mm-dd') ?? '')
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_SEQUENCE,data.sequence ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ACOUNT_ID,data.account_id ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ACCOUNT_NAME,data.account_name ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ID_TYPE,data.id_type ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_NATIONALITY_CODE,data.nationality_code ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ID_NO,data.id_no ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ISSUE_DATE,dateFormat(data.id_issue_date,'yyyy-mm-dd') ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ID_ISSUE_PLACE,data.id_issue_place ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_EMAIL,data.email ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_PHONE,data.phone ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_ADDRESS,data.address ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_CITY,data.city ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_SHAREHOLDER_TYPE,data.shareholder_type ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_FIELD_OF_ACTIVITY,data.field_of_activity ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_BUSSINESS_TYPE,data.bussiness_type ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_PROFESSIONAL_REGISTRATION_CRITERIA,data.professional_registration_criteria ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_PINV,data.PINV ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_PROFESSIONAL_DATE_START,dateFormat(data.profesional_date_start,'yyyy-mm-dd') ?? '');
    result.SQL_CMD = result.SQL_CMD.replaceAll(VAR_PROFESSIONAL_DATE_END,dateFormat(data.profesional_date_end,'yyyy-mm-dd') ?? '');

    result.status = 'Success';
    result.content = finFile.content;

    //Return generated data
    return result;
}

async function test() {
    let data  = {
        sequence: '0004',
        account_id: '006C111111',
        account_name: 'NGUYỄN LÝ QUAN',
        id_type: '001',
        nationality_code: 'VN',
        id_no: '025564123589',
        id_issue_date: '2023-06-12',
        id_issue_place: 'Bộ công an TP.HCM',
        email: 'quannl@acbs.com.vn',
        phone: '0903313102',
        address: '32/2 Nguyễn Huy Lượng, P.14, Q.Bình Thạnh, TP.HCM',
        city: 'Thành Phố Hồ Chí Minh',
        shareholder_type: 'FORCORP',
        field_of_activity: '1',
        bussiness_type: '3',
        professional_registration_criteria: '11',
        PINV: 'SINV',
        profesional_date_start: '2023-03-12',
        profesional_date_end: '2023-12-01'
    };

    let fin = await toUnencodedDataFin(data);

    const encode = (content,decode_mode) => {
        let encode_decode_table = {
            "đ":"?dd?",
            "ă":"?aw?",
            "â":"?aa?",
            "ê":"?ee?",
            "ô":"?oo?",
            "ơ":"?ow?",
            "ư":"?uw?",
            "á":"?as?",
            "ắ":"?aws?",
            "ấ":"?aas?",
            "e":"?es?",
            "ế":"?ees?",
            "ố":"?oos?",
            "ớ":"?ows?",
            "ú":"?us?",
            "ứ":"?uws?",
            "í":"?is?",
            "à":"?af?",
            "ằ":"?awf?",
            "ầ":"?aaf?",
            "è":"?ef?",
            "ề":"?eef?",
            "ò":"?of?",
            "ồ":"?oof?",
            "ờ":"?owf?",
            "ù":"?uf?",
            "ừ":"?uwf?",
            "ì":"?if?",
            "ả":"?ar?",
            "ẳ":"?awr?",
            "ẩ":"?aar?",
            "ẻ":"?er?",
            "ể":"?eer?",
            "ỏ":"?or?",
            "ổ":"?oor?",
            "ở":"?owr?",
            "ủ":"?ur?",
            "ử":"?uwr?",
            "ỉ":"?ir?",
            "ã":"?ax?",
            "ẵ":"?awx?",
            "ẫ":"?aax?",
            "ẽ":"?ex?",
            "ễ":"?eex?",
            "õ":"?ox?",
            "ỗ":"?oox?",
            "ỡ":"?owx?",
            "ũ":"?ux?",
            "ữ":"?uwx?",
            "ĩ":"?ix?",
            "ạ":"?aj?",
            "ặ":"?awj?",
            "ậ":"?aaj?",
            "ẹ":"?ej?",
            "ệ":"?eej?",
            "ọ":"?oj?",
            "ộ":"?ooj?",
            "ợ":"?owj?",
            "ụ":"?uj?",
            "ự":"?uwj?",
            "ị":"?ij?",
            "@":"(at)",
            "Đ":"?DD?",
            "Ă":"?AW?",
            "Â":"?AA?",
            "Ê":"?EE?",
            "Ô":"?OO?",
            "Ơ":"?OW?",
            "Ư":"?UW?",
            "Á":"?AS?",
            "Ắ":"?AWS?",
            "Ấ":"?AAS?",
            "É":"?ES?",
            "Ế":"?EES?",
            "Ố":"?OOS?",
            "Ớ":"?OWS?",
            "Ú":"?US?",
            "Ứ":"?UWS?",
            "Í":"?IS?",
            "À":"?AF?",
            "Ằ":"?AWF?",
            "Ầ":"?AAF?",
            "È":"?EF?",
            "Ề":"?EEF?",
            "Ò":"?OF?",
            "Ồ":"?OOF?",
            "Ờ":"?OWF?",
            "Ù":"?UF?",
            "Ừ":"?UWF?",
            "Ì":"?IF?",
            "Ả":"?AR?",
            "Ẳ":"?AWR?",
            "Ẩ":"?AAR?",
            "Ẻ":"?ER?",
            "Ể":"?EER?",
            "Ỏ":"?OR?",
            "Ổ":"?OOR?",
            "Ở":"?OWR?",
            "Ủ":"?UR?",
            "Ử":"?UWR?",
            "Ỉ":"?IR?",
            "Ã":"?AX?",
            "Ẵ":"?AWX?",
            "Ẫ":"?AAX?",
            "Ẽ":"?EX?",
            "Ễ":"?EEX?",
            "Õ":"?OX?",
            "Ỗ":"?OOX?",
            "Ỡ":"?OWX?",
            "Ũ":"?UX?",
            "Ữ":"?UWX?",
            "Ĩ":"?IX?",
            "Ạ":"?AJ?",
            "Ặ":"?AWJ?",
            "Ậ":"?AAJ?",
            "Ẹ":"?EJ?",
            "Ệ":"?EEJ?",
            "Ọ":"?OJ?",
            "Ộ":"?OOJ?",
            "Ợ":"?OWJ?",
            "Ụ":"?UJ?",
            "Ự":"?UWJ?",
            "Ị":"?IJ?"        
        }
        for(let i = 0; i < Object.keys(encode_decode_table).length; i++) {
            if (decode_mode == true) {
                if (content.includes(encode_decode_table[Object.keys(encode_decode_table)[i]]) == true) {
                    content = content.replaceAll(encode_decode_table[Object.keys(encode_decode_table)[i]],Object.keys(encode_decode_table)[i]);
                }
            }
            else {
                if (content.includes(Object.keys(encode_decode_table)[i]) == true) {
                    content = content.replaceAll(Object.keys(encode_decode_table)[i],encode_decode_table[Object.keys(encode_decode_table)[i]]);
                }
            }
        }
        return content;
      };
    
    fin.content = encode(fin.content,false);
    console.log(fin);
}
//test();

module.exports = {
    /*fromDataFin,*/
    toUnencodedDataFin,
    selfCheck,
    getSqlAdditionalData,
    EXCEL_TEMPLATE,
    TEMPLATE_FILE_NAME,
    EXCEL
}